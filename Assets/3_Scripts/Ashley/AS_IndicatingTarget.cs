﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AS_IndicatingTarget : MonoBehaviour
{
  public enum TargetPivot
  {
    Center = 0,
    Top,
    TopLeft,
    Left,
    BottomLeft,
    Bottom,
    BottomRight,
    Right,
    TopRight
  }
  public TargetPivot pivot;
  public Vector3 offset;
  public Vector2 targetSize;
  public Color targetColor;
  public bool useLockBox;
  public bool useArrow;

}
