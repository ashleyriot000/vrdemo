﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class AS_Indicator : MonoBehaviour
{
  public Camera mainCam;
  public AS_IndicatingTarget m_target;
  public Image targetImage;
  public Sprite lockBox;
  public Sprite arrow;

  [Range(0.1f, 0.9f)]
  public float maxRangeX;
  [Range(0.1f, 0.9f)]
  public float maxRangeY;


  private float centerX;
  private float centerY;
  private Vector2 min;
  private Vector2 max;

  public Text message0;
  public Text message1;

  void Start()
  {
    mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
    var halfX = Screen.width * 0.5f;
    centerY = Screen.height * 0.5f;
    centerX = halfX * 0.5f;
    var range = new Vector2(centerX * maxRangeX, centerY * maxRangeY);
    Debug.Log(range);
    min.x = centerX - range.x;
    min.y = centerY - range.y;
    max.x = centerX + range.x;
    max.y = centerY + range.y;
  }

  void Update()
  {
    //위치 갱신
    targetImage.transform.position = m_target.transform.position;
    //스크린좌표로 변환
    var lPos = mainCam.WorldToScreenPoint(m_target.transform.position, Camera.MonoOrStereoscopicEye.Left);
    var rPos = mainCam.WorldToScreenPoint(m_target.transform.position, Camera.MonoOrStereoscopicEye.Right);

    //스크린 좌표상에 타겟이 범위안에 있으면

    var l = lPos.z > 0f && lPos.x > min.x && lPos.x < max.x
         && lPos.y > min.y && lPos.y < max.y;

    var r = rPos.z > 0f && rPos.x > min.x && rPos.x < max.x
         && rPos.y > min.y && rPos.y < max.y;
    if(l && r)
    {
      if (m_target.useLockBox)
      {
        message0.text = $"Inside {lPos}";
        message1.text = $"Inside {rPos}";
        targetImage.enabled = true;
        var rect = targetImage.GetComponent<RectTransform>();
        rect.sizeDelta = m_target.targetSize;

        targetImage.sprite = lockBox;
        targetImage.color = m_target.targetColor;

        targetImage.transform.rotation = Quaternion.LookRotation(transform.position - m_target.transform.position);
        Vector3 scale = targetImage.transform.localScale;
        scale.x = 1f;
        scale.y = 1f;
        scale.z = 1f;
        targetImage.transform.localScale = scale;
      }
    }
    else
    {
      if(l)
      {
        message0.text = $"Outside {lPos}";
      }
      if(r)
      {
        message1.text = $"Outside {rPos}";
      }
      targetImage.enabled = false;
    }




    // if (lPos.z > 0 && rPos.z > 0
    // && lPos.x > minLX && rPos.x > minRX
    // && lPos.x < maxLX && rPos.x < maxRX
    // && lPos.y > minY && rPos.y > minY
    // && lPos.y < maxY && rPos.y < maxY)
    // {
    //   minPos.text = $"minLX :{minLX}, maxLX :{maxLX}, LX:{lPos.x}";
    //   maxPos.text = $"minRX :{minRX}, maxRY :{maxRX}, RX:{rPos.x}";
    //   lScreenPos.text = $"minY :{minY}, maxY :{maxY}";
    //   rScreenPos.text = $"LY :{lPos.y}, RY:{rPos.y}";
    //   if (m_target.useLockBox)
    //   {
    //     var rect = image.GetComponent<RectTransform>();
    //     rect.sizeDelta = m_target.targetSize;

    //     image.sprite = lockBox;
    //     image.color = m_target.targetColor;

    //     image.transform.rotation = Quaternion.LookRotation(transform.position - m_target.transform.position);
    //     Vector3 scale = image.transform.localScale;
    //     scale.x = 1f;
    //     scale.y = 1f;
    //     scale.z = 1f;
    //     image.transform.localScale = scale;
    //   }
    //   else
    //   {
    //     image.color = Color.clear;
    //     image.sprite = null;
    //   }
    // }
    // else
    // {
    //   minPos.text = $"minLX :{minLX}, maxLX :{maxLX}, LX:{lPos.x}";
    //   maxPos.text = $"minRX :{minRX}, maxRY :{maxRX}, RX:{rPos.x}";
    //   lScreenPos.text = $"minY :{minY}, maxY :{maxY}";
    //   rScreenPos.text = $"LY :{lPos.y}, RY:{rPos.y}";
    //   if (m_target.useArrow)
    //   {
    //     //뒤에 있는지 확인
    //     var targetDirect = m_target.transform.position - transform.position;
    //     if (Vector3.Dot(targetDirect, transform.forward) < 0f)
    //     {
    //       rPos.z = -rPos.z;
    //       lPos.z = -lPos.z;
    //       if (lPos.x > centerLX) lPos.x = minLX;
    //       else lPos.x = maxLX;
    //       if(rPos.x > centerRX) rPos.x = minRX;
    //       else rPos.x = maxRX;
    //     }
    //     //Y축을 범위안으로 고정
    //     if(rPos.y <= minY) rPos.y = minY;
    //     if(rPos.y >= maxY) rPos.y = maxY;
    //     if(lPos.y <= minY) lPos.y = minY;
    //     if(lPos.y >= maxY) lPos.y = maxY;

    //     //양쪽의 스크린좌표중 하나 선택하기
    //     var sPos = Vector3.zero;
    //     var wPos = Vector3.zero;
    //     if(lPos.x >= maxLX)
    //     {
    //       lPos.x = maxX;
    //       sPos = lPos;
    //       wPos = Camera.main.ScreenToWorldPoint(sPos, Camera.MonoOrStereoscopicEye.Right);
    //     }
    //     if(rPos.x <= minX)
    //     {
    //       rPos.x = minX;
    //       sPos = rPos;
    //       wPos = Camera.main.ScreenToWorldPoint(sPos, Camera.MonoOrStereoscopicEye.Left);
    //     }
    //     //화살표 회전
    //     image.transform.position = wPos;
    //     image.transform.rotation = Quaternion.LookRotation(transform.position - wPos);
    //     //화살표 사이즈 조정
    //     var rect = image.GetComponent<RectTransform>();
    //     rect.sizeDelta = arrowSize;
    //     Vector3 scale = image.transform.localScale;
    //     scale.x = sPos.z;
    //     scale.y = sPos.z;
    //     scale.z = sPos.z;
    //     image.transform.localScale = scale;
    //     image.sprite = arrow;
    //     image.color = m_target.targetColor;
    //   }
    //   else
    //   {
    //     image.color = Color.clear;
    //     image.sprite = null;
    //   }
    // }

    #region 
    //일반 디스플레이에서는 적용되지만 VR기기에서는 안됨.
    // var sPos = Camera.main.WorldToScreenPoint(m_target.transform.position);
    // var color = Color.white;

    // //타겟이 범위안에 있으면 
    // if (sPos.z > 0
    // && sPos.x > minX
    // && sPos.x < maxX
    // && sPos.y > minY
    // && sPos.y < maxY)
    // {
    //   if (m_target.useLockBox)
    //   {
    //     var rect = image.GetComponent<RectTransform>();
    //     rect.sizeDelta = m_target.targetSize;

    //     image.sprite = lockBox;
    //     image.color = m_target.targetColor;
    //     image.transform.position = m_target.transform.position;
    //     Vector3 scale = image.transform.localScale;
    //     scale.x = 1f;
    //     scale.y = 1f;
    //     scale.z = 1f;
    //     image.transform.localScale = scale;

    //   }
    //   else
    //   {
    //     image.color = Color.clear;
    //     image.sprite = null;
    //   }
    // }
    // else //범위밖이면
    // {
    //   if (m_target.useArrow)
    //   {
    //     var rect = image.GetComponent<RectTransform>();
    //     rect.sizeDelta = arrowSize;
    //     var targetDirect = m_target.transform.position - transform.position;
    //     var dist = targetDirect.magnitude;
    //     if (sPos.x > maxX) sPos.x = maxX;
    //     if (sPos.x < minX) sPos.x = minX;
    //     if (sPos.y > maxY) sPos.y = maxY;
    //     if (sPos.y < minY) sPos.y = minY;

    //     //뒤에 있다면
    //     if (Vector3.Dot(targetDirect, transform.forward) < 0)
    //     {
    //       sPos.z = -sPos.z;
    //       if (sPos.x > centerX) sPos.x = minX;
    //       else sPos.x = maxX;

    //       sPos.y = centerY * 2 - sPos.y;

    //     }
    //     var wPos = Camera.main.ScreenToWorldPoint(sPos);
    //     image.sprite = arrow;

    //     image.color = m_target.targetColor;
    //     image.transform.position = wPos;
    //     Vector3 scale = image.transform.localScale;
    //     scale.x = sPos.z;
    //     scale.y = sPos.z;
    //     scale.z = sPos.z;
    //     image.transform.localScale = scale;
    //   }
    // }
    #endregion
  }
}