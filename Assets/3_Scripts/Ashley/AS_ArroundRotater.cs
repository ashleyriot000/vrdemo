﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AS_ArroundRotater : MonoBehaviour
{

  public Transform target;

  void Start()
  {

  }

  void Update()
  {
    transform.position = target.position;
    var rotation = target.rotation.eulerAngles;
    rotation.z = 0;
    transform.rotation = Quaternion.Euler(rotation);
    transform.Translate(Vector3.forward * 8f);
  }
}
